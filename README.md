
# Neural Networking

This is a basic Neural Network created in Swift for Xcode.



## Usage

```swift
var layer1 = Layer(neuronCount: 5)

var layer2 = Layer(neuronCount: 3, previousLayer: lay1)

var layer3 = Layer(neuronCount: 1, previousLayer, layer2)

var layers = [ layer1, layer2, layer3 ]

var network = NeuralNetwork(NeuralNetwork(layers: layers, learningRate: 0.01))

lay1.neurons[0].value = 1
lay1.neurons[1].value = 0.5
lay1.neurons[2].value = 1
lay1.neurons[3].value = 0.5
lay1.neurons[4].value = 1

network.calculateNetwork()

network.train()

network.printConnectorWeights()
```

