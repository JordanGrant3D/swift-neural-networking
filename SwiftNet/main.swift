import Foundation
import Darwin
import os.log

var lay1 = Layer(neuronCount: 2)

var lay2 = Layer(neuronCount: 2, previousLayer: lay1)

var lay3 = Layer(neuronCount: 1, previousLayer: lay2)

var layers = [ lay1, lay2, lay3 ]

var inputs: [Float] = [ 1, 0.5 ]
var targets: [Float] = [ -0.5 ]

var net = NeuralNetwork(layers: layers, learningRate: 0.1)

net.train(inputValues: inputs, targetValues: targets)

var testInputs: [Float] = [ 1, 0.5 ]


net.calculateNetwork(inputValues: testInputs).forEach { value in
    print("output: \(value)")
    
}
net.printConnectorWeights()


public class NeuralNetwork
{
    
    func printConnectorWeights()
    {
        for layer in layers
        {
            for neuron in layer.neurons
            {
                for connector in neuron.startConnectors
                {
                    print("Weight: \(connector.weight)")
                }
            }
        }
    }
    
    let layers: [Layer]
    
    init(layers: [Layer], learningRate: Float) {
        self.layers = layers
        self.learningRate = learningRate
    }
    
    var learningRate: Float
    
    func train(inputValues: [Float], targetValues: [Float])
    {
        var error: Float = 0.0
        
        
        for layer in layers
        {
            for neuron in layer.neurons
            {
                
                for connector in neuron.startConnectors
                {
                    var previousError = error
                    
                    var _learningRate = learningRate
                    
                    connector.weight += learningRate
                    error = calculateNetworkError(inputValues: inputValues, targetValues: targetValues)
                    
                    if(error == previousError)
                    {
                        continue
                    }
                    
                    if (error > previousError)
                    {
                        _learningRate *= -1
                        connector.weight += _learningRate
                        error = calculateNetworkError(inputValues: inputValues, targetValues: targetValues)
                    }
                    
                    var errorCorrecton: Float = 0.0
                    
                    
                    
                    repeat
                    {
                        connector.weight += _learningRate
                        error = calculateNetworkError(inputValues: inputValues, targetValues: targetValues)
                        if errorCorrecton < 0
                        {
                            _learningRate *= -1
                        }
                        
                        connector.weight += _learningRate*2
                        error = calculateNetworkError(inputValues: inputValues, targetValues: targetValues)
                        
                        errorCorrecton = previousError - error
                        print("error \(error)")
                        
                        
                        error = calculateNetworkError(inputValues: inputValues, targetValues: targetValues)
                        errorCorrecton = previousError - error
                        
                        previousError = error
                    }
                    while(errorCorrecton > 0.01)
                    
                    
                }
            }
        }
    }
    
    
    
    
    func calculateNetwork(inputValues: [Float]) -> [Float]
    {
        for (index, neuron) in layers.first!.neurons.enumerated()
        {
            neuron.value = inputValues[index]
        }
        
        for layerIndex in 1..<layers.count {
            let layer = layers[layerIndex]
            
            for neuron in layer.neurons
            {
                neuron.value = 0.0
            }
            
            for neuron in layer.neurons
            {
                var result: Float = 0.0
                
                for connector in neuron.startConnectors
                {
                    result += connector.weight * connector.startNeuron.value
                }
                
                neuron.value = result
            }
        }
        
        return layers.last!.getNeuronValues()
    }
    
    private func calculateNetworkError(inputValues: [Float], targetValues: [Float]) -> Float
    {
        var copyLayers: [Layer] = []
        copyLayers.append(contentsOf: layers)
        
        for index in 0..<inputValues.count
        {
            copyLayers[0].neurons[index].value = inputValues[index]
        }
        
        
        
        for copyLayerIndex in 1..<copyLayers.count
        {
            let copyLayer = copyLayers[copyLayerIndex]
            for neuronIndex in 0..<copyLayer.neurons.count
            {
                copyLayer.neurons[neuronIndex].value = 0.0
            }
            
            for neuron in copyLayer.neurons
            {
                
                var result: Float = 0.0
                
                for connector in neuron.startConnectors
                {
                    result += connector.weight * connector.startNeuron.value
                }
                
                neuron.value = result
            }
        }
        
        return calculateError(targetValues: targetValues, network: copyLayers)
    }
    
    func calculateError(targetValues: [Float], network: [Layer]) -> Float
    {
        let outputValues = network.last!.getNeuronValues()
        
        guard outputValues.count == targetValues.count else {
            fatalError("Number Of Predictions And Target Values Must Be Equal.")
        }
        
        var result: Float = 0
        
        for index in 0..<outputValues.count {
            result += (outputValues[index] - targetValues[index])
        }
        
        return abs(result)
    }
    
}

public class Layer {
    var neurons: [Neuron] = [Neuron]()
    var previousLayer: Layer?
    
    init(neuronCount: Int, previousLayer: Layer? = nil) {
        self.previousLayer = previousLayer
        
        for _ in 0..<neuronCount {
            let neuron = Neuron()
            neurons.append(neuron)
            
            if let prevLayer = previousLayer {
                for (index, prevNeuron) in prevLayer.neurons.enumerated() {
                    let connector = Connector(startNeuron: prevNeuron, endNeuron: neuron, startNeuronIndex: index, endNeuronIndex: neurons.count - 1)
                    prevNeuron.endConnectors.append(connector)
                    neuron.startConnectors.append(connector)
                }
            }
        }
    }
    
    func getNeuronValues() -> [Float]
    {
        var result: [Float] = []
        
        neurons.forEach { neuron in
            result.append(neuron.value)
        }
        
        return result
    }
}


public class Neuron
{
    private var rawValue: Float = 0.0
    var startConnectors: [Connector] = [Connector]()
    var endConnectors: [Connector] = [Connector]()
    
    var value: Float
    {
        get
        {
            return rawValue
        }
        set(value)
        {
            rawValue = max(min(value, 1.0), -1.0)
        }
    }
}

public class Connector
{
    var weight: Float = 0.321
    
    let startNeuronIndex: Int
    let endNeuronIndex: Int
    
    init(startNeuron: Neuron, endNeuron: Neuron, startNeuronIndex: Int, endNeuronIndex: Int) {
        self.startNeuronIndex = startNeuronIndex
        self.endNeuronIndex = endNeuronIndex
        self.startNeuron = startNeuron
        self.endNeuron = endNeuron
    }
    
    let startNeuron: Neuron
    
    let endNeuron: Neuron
}
